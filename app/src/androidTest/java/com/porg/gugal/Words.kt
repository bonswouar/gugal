/*
 *     Words.kt
 *     Gugal
 *     Copyright (c) 2023 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal

class Words {
    companion object {
        private val ALL_WORDS = listOf(
            "adds", "dead", "coastal", "regulation", "purple", "everywhere", "flu", "audio", "pic", "diet",
            "skin", "invision", "florist", "curves", "commodity", "humans", "rounds", "test", "accent", "poultry",
            "confirmation", "milton", "surely", "radar", "south", "meetings", "alerts", "lots", "concrete",
            "afford", "hundreds", "websites", "tonight", "scroll", "township", "losses", "judy", "join", "sublime", "gain")

        fun listOfWords(): List<String> {
            val sublist = ALL_WORDS.shuffled().subList(0, 15).toMutableList()
            for (i in sublist.indices) {
                sublist[i] += " (index = $i)"
            }
            return sublist
        }
    }
}