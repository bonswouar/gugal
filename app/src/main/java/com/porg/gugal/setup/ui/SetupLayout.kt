/*
 *     SetupLayout.kt
 *     Gugal
 *     Copyright (c) 2023 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.setup.ui

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.*
import androidx.compose.material3.windowsizeclass.WindowSizeClass
import androidx.compose.material3.windowsizeclass.WindowWidthSizeClass
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp

/**
 * A large header with large text and optionally a Back button.
 *
 * Note that use of this component is discouraged in contexts other than dual pane layouts for tablets.
 * In all other cases please use the official Material 3 LargeTopAppBar instead (requires some changes to the layout).
 *
 * @param text the title.
 * @param doFinish the action to do when the Back button is pressed, if the Back button is enabled. Can be null if the Back button isn't enabled.
 * @param showBackButton whether to show the Back button. Defaults to true.
 */
@Composable
fun SetupPermaExpandedHeader(text: String, doFinish: (() -> Unit)?, showBackButton: Boolean = true) {
    if (showBackButton)
        IconButton(
            onClick  = doFinish!!,
            modifier = Modifier.
            then(Modifier.padding(start = 16.dp, top = 16.dp, bottom = 0.dp, end = 16.dp))
        ) {
            Icon(
                imageVector = Icons.Filled.ArrowBack,
                contentDescription = "Go back",
            )
        }
    Text(
        text = text,
        modifier = Modifier
            .padding(start = 24.dp, top = 72.dp, bottom = 24.dp, end = 24.dp)
            .fillMaxWidth(),
        style = MaterialTheme.typography.displaySmall
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SetupLayout(
    windowSizeClass: WindowSizeClass,
    title: String,
    onBackButtonClick: () -> Unit,
    content: @Composable (PaddingValues, Boolean, Modifier) -> Unit
) {
    val scrollBehavior = TopAppBarDefaults.exitUntilCollapsedScrollBehavior()
    if (windowSizeClass.widthSizeClass == WindowWidthSizeClass.Compact)
        Scaffold(
            modifier = Modifier.nestedScroll(scrollBehavior.nestedScrollConnection),
            topBar = {
                LargeTopAppBar(
                    title = {
                        Text(
                            title,
                            maxLines = 1,
                            overflow = TextOverflow.Ellipsis
                        )
                    },
                    navigationIcon = {
                        IconButton(onClick = onBackButtonClick) {
                            Icon(
                                imageVector = Icons.Filled.ArrowBack,
                                contentDescription = "Go back",
                            )
                        }
                    },
                    scrollBehavior = scrollBehavior
                )
            },
            content = { innerPadding ->
                content(innerPadding, false, Modifier)
            }
        )
    else
        Row(Modifier.fillMaxHeight()) {
            SetupPermaExpandedHeader(
                text = title,
                doFinish = onBackButtonClick,
                //modifier = Modifier.weight(1.0f).width((LocalConfiguration.current.screenWidthDp / 2).dp)
            )
            content(PaddingValues(16.dp), true, Modifier.weight(1.0f))
        }
}