/*
 *     SetupNewsActivity.kt
 *     Gugal
 *     Copyright (c) 2023 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.setup

import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.navigationBarsPadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.LargeTopAppBar
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberModalBottomSheetState
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.core.view.WindowCompat
import androidx.lifecycle.viewmodel.compose.viewModel
import com.porg.gugal.Global
import com.porg.gugal.MainActivity
import com.porg.gugal.R
import com.porg.gugal.news.NSViewModel
import com.porg.gugal.ui.theme.GugalTheme
import com.porg.m3.TwoButtonsRaw
import com.porg.m3.settings.CheckboxSetting
import com.porg.m3.settings.RegularSetting
import com.porg.m3.settings.SettingsTitle
import kotlinx.coroutines.launch

class SetupNewsActivity: ComponentActivity() {
    @OptIn(
        ExperimentalAnimationApi::class,
        ExperimentalMaterial3Api::class, ExperimentalMaterialApi::class
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val ctx = this

        WindowCompat.setDecorFitsSystemWindows(window, false)

        setContent {
            val feedList: MutableList<String> = remember {mutableStateListOf()}
            var openSheet by rememberSaveable { mutableStateOf(false) }
            val customFeedList: MutableList<String> = remember {mutableStateListOf()}
            val scope = rememberCoroutineScope()
            val bottomSheetState = rememberModalBottomSheetState(
                skipPartiallyExpanded = false
            )

            // load the old feeds (if any)
            val oldFeeds = Global.sharedPreferences.getStringSet("newsFeeds", setOf())!!
            feedList.addAll(oldFeeds)

            val vm: NSViewModel = viewModel()
            val vmState by vm.uiState.collectAsState()
            val scrollBehavior = TopAppBarDefaults.exitUntilCollapsedScrollBehavior()

            vm.createQueue(ctx)

            GugalTheme {
                Scaffold(
                    modifier = Modifier.nestedScroll(scrollBehavior.nestedScrollConnection),
                    topBar = {
                        LargeTopAppBar(
                            title = {
                                Text(
                                    getText(R.string.setup_feeds_title).toString(),
                                    maxLines = 1,
                                    overflow = TextOverflow.Ellipsis
                                )
                            },
                            navigationIcon = {
                                IconButton(onClick = { finish() }) {
                                    Icon(
                                        imageVector = Icons.Filled.ArrowBack,
                                        contentDescription = "Go back",
                                    )
                                }
                            },
                            scrollBehavior = scrollBehavior
                        )
                    },
                    content = { innerPadding ->
                        LazyColumn(
                            modifier = Modifier
                                .padding(innerPadding)
                                .fillMaxSize()
                        ) {
                            item {
                                SettingsTitle(getString(R.string.setup_feeds_subtitle_recs))
                            }
                            items(
                                vmState.feeds,
                                key = { feed -> feed.url}
                            ) { feed ->
                                CheckboxSetting(
                                    title = feed.name,
                                    body = feed.url,
                                    checkedProvider = {feedList.contains(feed.url) || customFeedList.contains(feed.url)},
                                    onCheckedChange = {x ->
                                        if (x) feedList.add(feed.url)
                                        else feedList.remove(feed.url)
                                    }
                                )
                            }
                            item {
                                Text(
                                    text = getString(R.string.setup_feeds_rec_desc),
                                    modifier = Modifier.padding(com.porg.m3.settings.SettingsPadding)
                                )
                                SettingsTitle(getString(R.string.setup_feeds_subtitle_custom))
                            }
                            items(customFeedList) { feed ->
                                RegularSetting(
                                    title = feed,
                                    body = ""
                                ) {
                                    customFeedList.remove(feed)
                                }
                            }
                            item {
                                RegularSetting(
                                    title = getString(R.string.setup_feeds_add_title),
                                    body = getString(R.string.setup_feeds_add_desc)
                                ) {
                                    openSheet = true
                                }
                            }
                        }
                    },
                    bottomBar = {
                        TwoButtonsRaw(
                            positiveAction = {
                                with(Global.sharedPreferences.edit()) {
                                    this.putStringSet(
                                        "newsFeeds",
                                        (feedList + customFeedList).toSet()
                                    )
                                    apply()
                                }
                                if (intent.getBooleanExtra("launchedDirectly", false)) {
                                    finish()
                                } else if (intent.getBooleanExtra("restartMain", false)) {
                                    val intent = Intent(ctx, MainActivity::class.java)
                                    startActivity(intent)
                                    finish()
                                } else {
                                    val intent = Intent(ctx, SetupFOSSActivity::class.java)
                                    startActivity(intent)
                                }
                            },
                            positiveText = getString(R.string.btn_next),
                            negativeAction = {
                                finish()
                            },
                            disablePositive = vmState.feeds.isEmpty(),
                            modifier = Modifier.navigationBarsPadding()
                        )
                    }
                )
                if (openSheet) {
                    ModalBottomSheet(
                        onDismissRequest = { openSheet = false },
                        sheetState = bottomSheetState,
                    ) {
                        var text by remember { mutableStateOf("") }
                        var label by remember { mutableStateOf(getString(R.string.setup_feeds_bs_field)) }
                        var error by remember { mutableStateOf(false) }
                        Column(Modifier.fillMaxWidth(), horizontalAlignment = Alignment.CenterHorizontally) {
                            Icon(
                                painter = painterResource(id = R.drawable.ic_news),
                                contentDescription = null,
                                modifier = Modifier.size(36.dp),
                                tint = MaterialTheme.colorScheme.primary
                            )
                            Text(
                                text = getString(R.string.setup_feeds_bs_title),
                                style = MaterialTheme.typography.headlineMedium,
                                modifier = Modifier.padding(16.dp)
                            )
                            OutlinedTextField(
                                value = text,
                                onValueChange = { text = it },
                                label = {Text(label)},
                                singleLine = true,
                                isError = error,
                                modifier = Modifier.padding(horizontal=16.dp).fillMaxWidth()
                            )
                            Text(getString(R.string.setup_feeds_bs_description),
                                modifier = Modifier.padding(16.dp))
                            Button(
                                // Note: If you provide logic outside of onDismissRequest to remove the sheet,
                                // you must additionally handle intended state cleanup, if any.
                                onClick = {
                                    if (!Patterns.WEB_URL.matcher(text).matches()) {
                                        label = getString(R.string.setup_feeds_bs_noturl)
                                        error = true
                                    } else if (customFeedList.contains(text)) {
                                        label = getString(R.string.setup_feeds_bs_exists)
                                        error = true
                                    } else {
                                        customFeedList.add(text)
                                        scope.launch { bottomSheetState.hide() }.invokeOnCompletion {
                                            if (!bottomSheetState.isVisible) {
                                                openSheet = false
                                            }
                                        }
                                    }
                                },
                                modifier = Modifier.padding(bottom=16.dp)
                            ) {
                                Text("Add feed")
                            }
                        }
                    }
                }
            }
            if (vmState.feeds.isEmpty())
                vm.loadFeeds()
        }
    }
}