/*
 *     SetupFOSSActivity.kt
 *     Gugal
 *     Copyright (c) 2022 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.setup

import android.content.ComponentName
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.RequiresApi
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.core.view.WindowCompat
import com.porg.gugal.MainActivity
import com.porg.gugal.R
import com.porg.gugal.ui.theme.GugalTheme
import com.porg.m3.Tip
import com.porg.m3.TwoButtons

class SetupFOSSActivity : ComponentActivity() {
    @OptIn(ExperimentalMaterialApi::class,
        ExperimentalAnimationApi::class, ExperimentalMaterial3Api::class
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val ctx = this
        WindowCompat.setDecorFitsSystemWindows(window, false)
        setContent {
            val scrollBehavior = TopAppBarDefaults.exitUntilCollapsedScrollBehavior()
            GugalTheme {
                Scaffold(
                    modifier = Modifier.nestedScroll(scrollBehavior.nestedScrollConnection),
                    topBar = {
                        LargeTopAppBar(
                            title = {
                                Text(
                                    getText(R.string.setup_p4_title).toString(),
                                    maxLines = 1,
                                    overflow = TextOverflow.Ellipsis
                                )
                            },
                            navigationIcon = {
                                IconButton(onClick = { finish() }) {
                                    Icon(
                                        imageVector = Icons.Filled.ArrowBack,
                                        contentDescription = "Go back",
                                    )
                                }
                            },
                            scrollBehavior = scrollBehavior
                        )
                    },
                    content = { innerPadding ->
                        Surface(color = MaterialTheme.colorScheme.background) {
                            Column(modifier = Modifier
                                .padding(innerPadding)
                                .verticalScroll(rememberScrollState())
                                .fillMaxSize()
                            ) {
                                Text(
                                    text = getText(R.string.setup_p4_description).toString(),
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(horizontal = 16.dp, vertical = 8.dp),
                                    style = MaterialTheme.typography.bodyLarge
                                )
                                Tip(
                                    text = getText(R.string.setup_p4_tip_1).toString(),
                                    icon = R.drawable.ic_warning,
                                    modifier = Modifier.padding(horizontal = 16.dp, vertical = 8.dp),
                                    onClick = {
                                        val intent = Intent(Intent.ACTION_VIEW,
                                            Uri.parse("https://gugal.gitlab.io/why-official.html"))
                                        startActivity(Intent.createChooser(intent, "Open notice in"))
                                    }
                                )
                                // On Android 12 default handlers aren't assigned automatically, so we need an extra tip
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                                    Tip(text = getText(R.string.setup_p4_tip_3).toString(),
                                        modifier = Modifier.padding(horizontal = 16.dp, vertical = 8.dp),
                                        icon = R.drawable.ic_search,
                                        onClick = {
                                            val intent = Intent(Intent.ACTION_VIEW,
                                                Uri.parse("https://gugal.gitlab.io/launchers.html"))
                                            startActivity(Intent.createChooser(intent, "Open notice in"))
                                        }
                                    )
                                }
                            }
                        }
                        TwoButtons(
                            positiveAction = {
                                val intent = Intent(ctx, MainActivity::class.java)
                                intent.component =
                                    ComponentName("com.porg.gugal", "com.porg.gugal.MainActivity")
                                startActivity(intent)
                            },
                            positiveText = getString(R.string.btn_next),
                            negativeAction = {
                                finish()
                            }
                        )
                    }
                )
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.S)
    private fun openUrlHandlerActivity() {
        // One UI 4 doesn't have the "open by default" activity for some reason
        if (Build.MANUFACTURER.equals("samsung", ignoreCase = true)) {
            val intent = Intent()
            intent.action = "android.settings.MANAGE_DOMAIN_URLS"
            startActivity(intent)
        } else {
            val intent = Intent()
            intent.action = Settings.ACTION_APP_OPEN_BY_DEFAULT_SETTINGS
            intent.data = Uri.parse("package:$packageName")
            startActivity(intent)
        }
    }
}