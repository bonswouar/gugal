/*
 *     SetupConfigureSerpActivity.kt
 *     Gugal
 *     Copyright (c) 2022 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.setup

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.*
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.core.view.WindowCompat
import com.porg.gugal.BuildConfig
import com.porg.gugal.Global.Companion.serpProvider
import com.porg.gugal.Global.Companion.sharedPreferences
import com.porg.gugal.R
import com.porg.gugal.providers.SerpProvider
import com.porg.gugal.ui.theme.GugalTheme
import com.porg.m3.TwoButtons

class SetupConfigureSerpActivity : ComponentActivity() {

    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val ctx = this
        WindowCompat.setDecorFitsSystemWindows(window, false)

        // check if launched from settings
        val launchedFromSettings = intent.getBooleanExtra("launchedFromSettings", false)

        setContent {

            val showNext = remember { mutableStateOf(false) }
            val sn by showNext

            val scrollBehavior = TopAppBarDefaults.exitUntilCollapsedScrollBehavior()

            GugalTheme {
                Scaffold(
                    modifier = Modifier.nestedScroll(scrollBehavior.nestedScrollConnection),
                    topBar = {
                        LargeTopAppBar(
                            title = {
                                Text(
                                    getText(R.string.setup_p3_title).toString(),
                                    maxLines = 1,
                                    overflow = TextOverflow.Ellipsis
                                )
                            },
                            navigationIcon = {
                                IconButton(onClick = { finish() }) {
                                    Icon(
                                        imageVector = Icons.Filled.ArrowBack,
                                        contentDescription = "Go back",
                                    )
                                }
                            },
                            scrollBehavior = scrollBehavior
                        )
                    },
                    content = { innerPadding ->
                        Surface(color = MaterialTheme.colorScheme.background,
                                modifier = Modifier.padding(innerPadding)
                                    .verticalScroll(rememberScrollState())) {
                            serpProvider.ConfigComposable(
                                modifier = Modifier.fillMaxSize().padding(horizontal = 16.dp, vertical = 8.dp),
                                enableNextButton = showNext,
                                context = this
                            )
                        }
                        TwoButtons(
                            positiveAction = { saveSensitive(serpProvider, ctx) },
                            positiveText = getString(R.string.btn_save),
                            negativeAction = { finish() },
                            showNegative = !launchedFromSettings,
                            disablePositive = !sn
                        )
                    }
                )
            }
        }
    }

    private fun saveSensitive(serpProvider: SerpProvider, ctx: Context) {
        // Get the sensitive data
        val sensitive = serpProvider.getSensitiveCredentials()

        with (sharedPreferences.edit()) {
            // Edit the user's shared preferences
            for (i in sensitive) {
                this.putString("serp_${serpProvider.id}_${i.key}", i.value)
            }
            // Save the last Gugal version to not show the changelog again
            this.putInt("lastGugalVersion", BuildConfig.VERSION_CODE)
            apply()
        }

        val intent = Intent(ctx, SetupFOSSActivity::class.java)
        startActivity(intent)
    }

}