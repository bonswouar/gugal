/*
 *     URLs.kt
 *     Gugal
 *     Copyright (c) 2023 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 This file contains various URLs used throughout Gugal.
 Developers of forks are encouraged to change the URLs here to point to their sites.
 */

package com.porg.gugal

/**
 * The URL to load suggested feeds from in the News setup page.
 */
const val SUGGESTED_FEED_LIST_URL = "https://gitlab.com/gugal/feeds/-/raw/main/feeds_min.json"

/**
 * The site to report issues to. Shown in the About page if the current Gugal version is a preview build.
 *
 * Please change this URL before releasing a fork; upstream Gugal isn't accepting issues in
 * forks, especially if the issue isn't present in upstream.
 */
const val PREVIEW_ISSUE_URL = "https://gitlab.com/narektor/gugal/-/issues/new?issue[description]=/label%20~beta%20%3C!--%20write%20below%20this%20line%20--%3E"

/**
 * The site to report issues to. Shown in the About page.
 *
 * Please change this URL before releasing a fork; upstream Gugal isn't accepting issues in
 * forks, especially if the issue isn't present in upstream.
 */
const val ISSUE_URL = "https://gitlab.com/narektor/gugal/-/issues/new"

/**
 * The site to report issues with News to. Shown in the News page.
 *
 * Please change this URL before releasing a fork; upstream Gugal isn't accepting issues in
 * forks, especially if the issue isn't present in upstream.
 */
const val NEWS_ISSUE_URL = "https://gitlab.com/narektor/gugal/-/issues/27"

/**
 * The URL of the app's repository.
 *
 * Please change this URL before releasing a fork so your users know where to find the source code.
 */
const val REPO_URL = "https://gitlab.com/narektor/gugal"

/**
 * The URL of the owner's or project's donation page.
 */
const val DONATE_URL = "https://ko-fi.com/thegreatporg"

/**
 * The URL of the app's translation page (or instructions for translating).
 */
const val TRANSLATE_URL = "https://hosted.weblate.org/engage/gugal/"

/**
 * The URL to load a given version's changelog from if the current Gugal version is a preview build.
 *
 * %d is substituted with the version code.
 */
const val PREVIEW_CHANGELOG_URL = "https://gitlab.com/gugal/beta/-/raw/main/changelogs/%d.txt"

/**
 * The URL to load a given version's changelog from.
 *
 * %d is substituted with the version code.
 */
const val CHANGELOG_URL = "https://gitlab.com/narektor/gugal/-/raw/main/fastlane/metadata/android/en-US/app_changelogs/%d.txt"