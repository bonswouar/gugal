/*
 *     ProviderInfo.kt
 *     Gugal
 *     Copyright (c) 2021 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.providers

/**
 * Information about a search engine.
 *
 * @param name the ID of the string with the name of this search engine.
 * @param description the ID of the string with a short description.
 * @param titleInSearchBox the ID of the string with a shorter name that will be shown in the app's search box *(Search (NAME))*.
 * @param requiresSetup does this SERP provider need setup? (Defaults to false)
 */
data class ProviderInfo(val name: Int, val description: Int, var titleInSearchBox: Int, var requiresSetup: Boolean = false) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ProviderInfo

        if (name != other.name) return false
        if (description != other.description) return false
        if (titleInSearchBox != other.titleInSearchBox) return false
        if (requiresSetup != other.requiresSetup) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + description.hashCode()
        result = 31 * result + titleInSearchBox.hashCode()
        result = 31 * result + requiresSetup.hashCode()
        return result
    }
}