/*
 *     NewsPage.kt
 *     Gugal
 *     Copyright (c) 2022 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.news

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import androidx.lifecycle.viewmodel.compose.viewModel
import com.android.volley.toolbox.Volley
import com.porg.gugal.Global
import com.porg.gugal.R
import com.porg.gugal.ui.cardElevation
import com.porg.gugal.ui.theme.GugalTheme
import com.porg.gugal.ui.theme.shapeScheme
import com.porg.gugal.viewmodel.NewsViewModel
import java.text.SimpleDateFormat
import java.util.Date

val dateFormat: SimpleDateFormat = SimpleDateFormat("EEE, MMM d, yyyy")

@Composable
fun NewsPage(context: Context?, newsModel: NewsViewModel = viewModel()) {
    val newsState by newsModel.uiState.collectAsState()
    val queue = Volley.newRequestQueue(context)
    newsModel.setQueue(queue)
    if (newsState.articles.isEmpty()) {
        val feeds = Global.sharedPreferences.getStringSet("newsFeeds", null)
        if (feeds == null) {
            Text(stringResource(R.string.news_feeds_error))
            return
        }
        newsModel.setFeeds(feeds.toList())
        newsModel.loadArticles()
    }

    Articles(newsState.articles, { newsState.loaded }, context = context)
}

@Composable
fun NewsHeader() {
    Text(
        text = stringResource(R.string.news_header_title),
        modifier = Modifier.padding(start = 14.dp, end = 14.dp, top = 40.dp, bottom = 30.dp),
        style = MaterialTheme.typography.displaySmall
    )
    /*Text(
        text = stringResource(R.string.news_header_date_placeholder),
        modifier = Modifier.padding(start = 14.dp, end = 14.dp, bottom = 30.dp),
        style = MaterialTheme.typography.bodyLarge
    )*/
}

@Composable
fun ArticleCard(art: Article, context: Context?) {
    Log.d("gugalNews", art.body)
    Surface(
        shape = MaterialTheme.shapeScheme.medium,
        tonalElevation = cardElevation,
        modifier = Modifier
            .padding(start = 14.dp, end = 14.dp, top = 4.dp, bottom = 8.dp)
            .fillMaxWidth()
            .clickable(onClick = {
                if (art.url != "") {
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(art.url))
                    // Note the Chooser below. If no applications match,
                    // Android displays a system message.So here there is no need for try-catch.
                    ContextCompat.startActivity(
                        context!!,
                        Intent.createChooser(intent, context.getString(R.string.news_chooser)),
                        null
                    )
                }
            }),
    ) {
        Column {
            Text(
                text = art.title,
                modifier = Modifier.padding(all = 14.dp),
                style = MaterialTheme.typography.titleLarge
            )
            Text(
                text = art.description,
                modifier = Modifier.padding(bottom = 14.dp, start = 14.dp, end = 14.dp),
                style = MaterialTheme.typography.bodyLarge
            )
            if (art.author != "") {
                val txt =
                    if (art.publishDate != null)
                        stringResource(R.string.news_author_date).format(art.author, dateFormat.format(art.publishDate))
                    else
                        art.author
                Text(
                    text = txt,
                    modifier = Modifier.padding(bottom = 2.dp, start = 14.dp, end = 14.dp),
                    style = MaterialTheme.typography.titleSmall
                )
            }
            // Add a vertical space between the author and message texts
            Spacer(modifier = Modifier.height(14.dp))
        }
    }
}

@Composable
fun Articles(articles: List<Article>, loaded: () -> Boolean, context: Context?) {
    LazyColumn {
        item(key="NPArtHeader") {
            NewsHeader()
        }
        item(key="NPArtProg") {
            if (!loaded())
                LinearProgressIndicator(Modifier.fillMaxWidth())
        }
        items(articles, key = {x -> x.hashCode()}) { article ->
            ArticleCard(article, context)
        }
    }
}

@Composable
@Preview
fun PreviewArticle() {
    GugalTheme(ignoreThemePref = true) {
        ArticleCard(
            art = Article(
                "Gugal - an open source search client",
                "Reviewing an open source app that helps you google without the Google app",
                "https://news.example.com/stories/2023/01/01/gugal",
                "John Smith",
                mutableListOf(),
                Date(2023, 1, 1),
                "This week, we came across <i>Gugal</i>. This app describes itself as '[a] clean, lightweight FOSS search app."
            ),
            context = null
        )
    }
}