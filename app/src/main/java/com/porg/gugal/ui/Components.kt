/*
 *     Components.kt
 *     Gugal
 *     Copyright (c) 2022 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.ui

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.browser.customtabs.CustomTabsIntent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.windowsizeclass.WindowWidthSizeClass
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import com.porg.gugal.Global
import com.porg.gugal.R
import com.porg.gugal.data.Result
import com.porg.gugal.ui.theme.GugalTheme
import com.porg.gugal.ui.theme.shapeScheme
import com.porg.gugal.viewmodel.ResultViewModel

class Components {
    companion object {
        /**
         * A medium Gugal logo.
         *
         * @param modifier a modifier to be applied, optional.
         */
        @Composable
        fun GugalLogo(modifier: Modifier = Modifier, size: Dp = 96.dp, padding: Dp = 48.dp) {
            val bg = MaterialTheme.colorScheme.primary
            val fg = MaterialTheme.colorScheme.onPrimary
            androidx.compose.material.Card(
                modifier = Modifier
                    .padding(all = padding)
                    .size(size)
                    .then(modifier),
                shape = CircleShape,
                backgroundColor = bg
            ) {
                Image(
                    painterResource(R.drawable.ic_launcher_foreground),
                    contentDescription = stringResource(R.string.desc_logo),
                    contentScale = ContentScale.Crop,
                    modifier = Modifier.fillMaxSize(),
                    colorFilter = ColorFilter.tint(color = fg)
                )
            }
        }

        /**
         * A clickable card that shows a search result's title, URL and description.
         *
         * @param res the Result to show.
         * @param context the context of the calling activity, optional (only required for clickable results).
         */
        @Composable
        fun ResultCardV2(res: Result, context: Context?, modifier: Modifier = Modifier) {
            Surface(
                shape = MaterialTheme.shapeScheme.medium,
                tonalElevation = cardElevation,
                modifier = Modifier
                    .padding(all = 4.dp)
                    .fillMaxWidth()
                    .clickable(onClick = {
                        if (res.url != "") openBrowser(context!!, res.url)
                    })
                    .then(modifier),
            ) {
                Column(modifier = Modifier.padding(all = 8.dp)) {
                    if (res.domain != "") {
                        Text(
                            text = res.domain,
                            modifier = Modifier.padding(all = 2.dp),
                            style = MaterialTheme.typography.titleSmall
                        )
                    }
                    Text(
                        text = res.title,
                        modifier = Modifier.padding(all = 2.dp),
                        style = MaterialTheme.typography.titleLarge
                    )
                    if (res.body != null) {
                        Text(
                            text = res.body,
                            modifier = Modifier.padding(all = 2.dp),
                            style = MaterialTheme.typography.bodyLarge
                        )
                    }
                }
            }
        }

        @Composable
        @Preview
        fun PreviewResultCardV2() {
            GugalTheme {
                ResultCardV2(
                    res = Result(
                        "Gugal",
                        "A clean, lightweight, FOSS web search app.",
                        "https://gitlab.com/narektor/gugal",
                        "gitlab.com"),
                    context = null
                )
            }
        }

        /**
         * A list of ResultCards.
         *
         * @param results the `Result`s to show.
         * @param context the context of the calling activity, optional (only required for clickable results).
         * @param sizeClass the current device's size class.
         */
        @ExperimentalAnimationApi
        @Composable
        fun Results(
            results: List<Result>,
            context: Context?,
            sizeClass: WindowWidthSizeClass,
            resultModel: ResultViewModel? = null
        ) {
            if (sizeClass == WindowWidthSizeClass.Compact)
                LazyColumn {
                    items(results) { message ->
                        ResultCardV2(message, context)
                    }
                    if (Global.searchPages.enabled)
                        item { PageButtons(resultModel!!) }
                }
            else {
                if (Global.resultGrid.enabled)
                    LazyVerticalGrid(
                        columns = GridCells.Adaptive(minSize = 384.dp)
                    ) {
                        items(results) { message ->
                            ResultCardV2(message, context, Modifier.heightIn(min = 40.dp))
                        }
                        if (Global.searchPages.enabled)
                            item { PageButtons(resultModel!!) }
                    }
                else
                    LazyColumn(
                        Modifier.padding(horizontal = 96.dp)
                    ) {
                        items(results) { message ->
                            ResultCardV2(message, context)
                        }
                        if (Global.searchPages.enabled)
                            item { PageButtons(resultModel!!) }
                    }
            }
        }

        @ExperimentalAnimationApi
        @Composable
        @Preview
        fun ResultsPreview() {
            GugalTheme {
                Results(
                    results = List(size = 3) {
                        Result(
                            "Google privacy scandal",
                            "Google have been fined for antitrust once again, a few days after people bought more than 5 Pixels.",
                            "about:blank",
                            "test.com"
                        )
                    },
                    null,
                    WindowWidthSizeClass.Compact
                )
            }
        }

        private fun openBrowser(context: Context, url: String) {
            if (Global.custTabInResults.enabled) {
                val builder = CustomTabsIntent.Builder()
                // show website title
                builder.setShowTitle(true)
                // animation for enter and exit of tab
                builder.setStartAnimations(context, android.R.anim.fade_in, android.R.anim.fade_out)
                builder.setExitAnimations(context, android.R.anim.fade_in, android.R.anim.fade_out)
                // launch the passed URL
                builder.build().launchUrl(context, Uri.parse(url))
            } else {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                ContextCompat.startActivity(
                    context,
                    Intent.createChooser(intent, "Open result in"),
                    null
                )
            }
        }
    }
}