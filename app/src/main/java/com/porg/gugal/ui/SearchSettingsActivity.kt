/*
 *     SearchSettingsActivity.kt
 *     Gugal
 *     Copyright (c) 2023 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.ui

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.text.style.TextOverflow
import androidx.core.view.WindowCompat
import com.porg.gugal.R
import com.porg.gugal.setup.SetupConfigureSerpActivity
import com.porg.gugal.setup.SetupSelectSerpActivity
import com.porg.gugal.ui.theme.GugalTheme
import com.porg.m3.settings.RegularSetting

class SearchSettingsActivity: ComponentActivity() {
    @OptIn(
        ExperimentalMaterial3Api::class
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val that = this
        WindowCompat.setDecorFitsSystemWindows(window, false)
        setContent {
            val scrollBehavior = TopAppBarDefaults.exitUntilCollapsedScrollBehavior()
            GugalTheme {
                Scaffold(
                    modifier = Modifier.nestedScroll(scrollBehavior.nestedScrollConnection),
                    topBar = {
                        LargeTopAppBar(
                            title = {
                                Text(
                                    getText(R.string.setting_search_title).toString(),
                                    maxLines = 1,
                                    overflow = TextOverflow.Ellipsis
                                )
                            },
                            navigationIcon = {
                                IconButton(onClick = { finish() }) {
                                    Icon(
                                        imageVector = Icons.Filled.ArrowBack,
                                        contentDescription = "Go back",
                                    )
                                }
                            },
                            scrollBehavior = scrollBehavior
                        )
                    },
                    content = { innerPadding ->
                        Surface(color = MaterialTheme.colorScheme.background) {
                            Column(modifier = Modifier
                                .padding(innerPadding)
                                .verticalScroll(rememberScrollState())
                                .fillMaxSize()
                            ) {
                                RegularSetting(R.string.setting_cred_title,
                                    R.string.setting_cred_desc,
                                    onClick = {
                                        val intent = Intent(that, SetupConfigureSerpActivity::class.java)
                                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                                        intent.putExtra("launchedFromSettings", true)
                                        startActivity(intent, null)
                                    })
                                RegularSetting(
                                    R.string.setting_serp_title,
                                    R.string.setting_serp_desc,
                                    onClick = {
                                        val intent = Intent(that, SetupSelectSerpActivity::class.java)
                                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                                        intent.putExtra("launchedFromSettings", true)
                                        startActivity(intent, null)
                                    })
                            }
                        }
                    }
                )
            }
        }
    }
}