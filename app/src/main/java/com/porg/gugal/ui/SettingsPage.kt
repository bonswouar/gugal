/*
 *     SettingsPage.kt
 *     Gugal
 *     Copyright (c) 2023 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.ui

import android.app.Activity
import android.content.Intent
import android.os.Build
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat.startActivity
import com.porg.gugal.BuildConfig
import com.porg.gugal.Global
import com.porg.gugal.Global.Companion.THEME_DARK
import com.porg.gugal.Global.Companion.THEME_LIGHT
import com.porg.gugal.Global.Companion.THEME_SYSTEM
import com.porg.gugal.MainActivity
import com.porg.gugal.R
import com.porg.gugal.devopt.DevOptMainActivity
import com.porg.gugal.news.NewsSettingsActivity
import com.porg.m3.settings.RegularPreviewSetting
import com.porg.m3.settings.RegularSetting

@Composable
fun SettingsPage(context: Activity?) {
    Column(
        modifier = Modifier.verticalScroll(rememberScrollState())
    ) {
        val openThemeDialog = remember { mutableStateOf(false) }
        val openClearDialog = remember { mutableStateOf(false) }

        // Search settings
        RegularSetting(R.string.setting_search_title,
            R.string.setting_search_desc,
            onClick = {
                val intent = Intent(context, SearchSettingsActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(context!!, intent, null)
            })

        // News
        RegularPreviewSetting(stringResource(R.string.setting_news_title),
            stringResource(R.string.setting_news_desc),
            onClick = {
                val intent = Intent(context, NewsSettingsActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(context!!, intent, null)
            }
        )

        // Clear search history
        if (Global.visibleSearchHistory.isNotEmpty())
            RegularSetting(
                stringResource(R.string.setting_clearHistory_title),
                stringResource(R.string.setting_clearHistory_desc),
                onClick = { openClearDialog.value = true })

        // Change theme
        RegularSetting(
            stringResource(R.string.setting_changeTheme_title),
            stringResource(R.string.setting_changeTheme_desc),
            onClick = { openThemeDialog.value = true })

        // Dev options (debug only)
        if (BuildConfig.VERSION_NAME.endsWith("-debug"))
            RegularSetting(
                stringResource(R.string.setting_devOpt_title),
                stringResource(R.string.setting_devOpt_desc),
                onClick = {
                    val intent = Intent(context, DevOptMainActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(context!!, intent, null)
                })

        // About setting
        RegularSetting(
            R.string.setting_about_title,
            R.string.setting_about_desc,
            onClick = {
                val intent = Intent(context, AboutActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(context!!, intent, null)
            })

        if (openClearDialog.value) {
            AlertDialog(
                onDismissRequest = {openClearDialog.value = false},
                title = {Text(text = stringResource(R.string.dialog_confirm))},
                text = {Text(text = stringResource(R.string.dialog_clearHistory))},
                confirmButton = {
                    TextButton(
                        onClick = {
                            // Clear search history in prefs
                            with (Global.sharedPreferences.edit()) {
                                remove("pastSearches")
                                remove("visiblePastSearches")
                                apply()
                            }
                            // Clear local copy
                            Global.visibleSearchHistory = mutableListOf()
                            Global.fullSearchHistory = mutableListOf()
                            // Close dialog
                            openClearDialog.value = false
                        }
                    ) {Text(stringResource(R.string.btn_yes))}
                },
                dismissButton = {
                    TextButton(
                        onClick = {openClearDialog.value = false}
                    ) {Text(stringResource(R.string.btn_no))}
                }
            )
        }
        if (openThemeDialog.value) ThemeDialog(openThemeDialog, context!!)

        Text(
            text = "Gugal " + BuildConfig.VERSION_NAME,
            modifier = Modifier.padding(start = 20.dp),
            style = MaterialTheme.typography.bodyMedium
        )
    }
}

@OptIn(ExperimentalAnimationApi::class, ExperimentalMaterialApi::class)
@Composable
fun ThemeDialog(state: MutableState<Boolean>, context: Activity) {
    var pref by remember {
        mutableStateOf(Global.sharedPreferences.getInt("themeOverride", THEME_SYSTEM))
    }
    AlertDialog(
        onDismissRequest = {state.value = false},
        title = {Text(text = stringResource(R.string.setting_changeTheme_title))},
        text = {Column {
            // Android 8-9 have a hidden dark mode, which may be visible on some OEM skins
            // (e.g. Samsung One UI). It works with Gugal's dark mode detection.
            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.O) {
                RadioListItem(
                    title = stringResource(R.string.dialog_theme_system_oreo),
                    selected = pref == THEME_SYSTEM,
                    onClick = {pref = THEME_SYSTEM}
                )
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                RadioListItem(
                    title = stringResource(R.string.dialog_theme_system),
                    selected = pref == THEME_SYSTEM,
                    onClick = {pref = THEME_SYSTEM}
                )
            }
            RadioListItem(
                title = stringResource(R.string.dialog_theme_dark),
                selected = pref == THEME_DARK,
                onClick = {pref = THEME_DARK}
            )
            RadioListItem(
                title = stringResource(R.string.dialog_theme_light),
                selected = pref == THEME_LIGHT,
                onClick = {pref = THEME_LIGHT}
            )
        }},
        confirmButton = {
            TextButton(
                onClick = {
                    with (Global.sharedPreferences.edit()) {
                        this.putInt("themeOverride", pref)
                        apply()
                    }
                    // Close dialog
                    state.value = false
                    // Restart the app
                    val intent = Intent(context, MainActivity::class.java)
                    context.startActivity(intent)
                    context.finishAffinity()
                }
            ) {Text(stringResource(android.R.string.ok))}
        }
    )
}

@Composable
fun RadioListItem(title: String, onClick: (() -> Unit), selected: Boolean) {
    Row(
        modifier = Modifier
            .clickable { onClick() }
            .fillMaxWidth()
            .padding(all = 0.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        RadioButton(
            selected = selected,
            onClick = onClick
        )
        Text(
            text = title,
            style = MaterialTheme.typography.bodyMedium
        )
    }
}