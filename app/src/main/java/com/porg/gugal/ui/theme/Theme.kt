package com.porg.gugal.ui.theme

import android.app.Activity
import android.os.Build
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalView
import androidx.core.view.WindowCompat
import com.porg.gugal.Global
import com.porg.gugal.Global.Companion.THEME_DARK
import com.porg.gugal.Global.Companion.THEME_LIGHT
import com.porg.gugal.Global.Companion.THEME_SYSTEM

val DarkColorPalette = darkColorScheme(
    primary = Blue300,
    secondary = Blue300
)

val LightColorPalette = lightColorScheme(
    primary = Blue500,
    secondary = Blue300

    /* Other default colors to override
    background = Color.White,
    surface = Color.White,
    onPrimary = Color.White,
    onSecondary = Color.Black,
    onBackground = Color.Black,
    onSurface = Color.Black,
    */
)

@Composable
fun GugalTheme(systemDarkTheme: Boolean = isSystemInDarkTheme(), ignoreThemePref: Boolean = false, content: @Composable() () -> Unit) {
    val context = LocalContext.current
    val dynamicColor = Build.VERSION.SDK_INT >= Build.VERSION_CODES.S

    var themeOverride = THEME_SYSTEM
    try {
        themeOverride =
            if (ignoreThemePref) THEME_SYSTEM
            else Global.sharedPreferences.getInt("themeOverride", THEME_SYSTEM)
    } catch (e: UninitializedPropertyAccessException) {
        // ignore it - the theme will be set to the system one
    }

    val darkTheme = systemDarkTheme || themeOverride == THEME_DARK
    val lightTheme = themeOverride == THEME_LIGHT
    val colors = when {
        dynamicColor && lightTheme -> dynamicLightColorScheme(context)
        dynamicColor && darkTheme -> dynamicDarkColorScheme(context)
        dynamicColor && !darkTheme -> dynamicLightColorScheme(context)
        darkTheme -> DarkColorPalette
        else -> LightColorPalette
    }


    MaterialTheme(
        colorScheme = colors,
        typography = Typography,
        content = content
    )

    val view = LocalView.current
    if (!view.isInEditMode) {
        SideEffect {
            val window = (view.context as Activity).window

            window.statusBarColor = Color.Transparent.toArgb()
            window.navigationBarColor = Color.Transparent.toArgb()

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                window.isNavigationBarContrastEnforced = false
            }

            val windowsInsetsController = WindowCompat.getInsetsController(window, view)

            windowsInsetsController.isAppearanceLightStatusBars = !darkTheme
            windowsInsetsController.isAppearanceLightNavigationBars = !darkTheme
        }
    }
}