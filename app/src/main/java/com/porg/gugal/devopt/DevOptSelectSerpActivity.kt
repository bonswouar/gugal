/*
 *     SetupSelectSerpActivity.kt
 *     Gugal
 *     Copyright (c) 2022 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.devopt

import android.content.SharedPreferences
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import com.porg.gugal.BuildConfig
import com.porg.gugal.Global.Companion.setSerpProvider
import com.porg.gugal.R
import com.porg.gugal.devopt.providers.AlwaysFailingProvider
import com.porg.gugal.ui.theme.GugalTheme
import com.porg.m3.TwoButtons
import com.porg.m3.settings.RadioSetting

class DevOptSelectSerpActivity : ComponentActivity() {
    @OptIn(
        androidx.compose.animation.ExperimentalAnimationApi::class
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val currentSerpProviderId = mutableStateOf("")
        setContent {
            GugalTheme {

                Warning()

                val _csp_id by currentSerpProviderId
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colorScheme.background) {
                    TwoButtons(
                        positiveAction = {
                            setSerpProvider(_csp_id)
                            saveSerp(_csp_id, true)
                            finish()
                        },
                        positiveText = getString(R.string.btn_next),
                        negativeAction = {
                            finish()
                        }
                    )
                    Column(
                        modifier = Modifier.fillMaxSize()
                    ) {
                        RadioSetting(
                            title = "Always fail",
                            body = "Always gives an error.",
                            selected = _csp_id == AlwaysFailingProvider.id,
                            onClick = {
                                currentSerpProviderId.value = AlwaysFailingProvider.id
                            }
                        )
                    }
                }
            }
        }
    }

    @Composable
    fun Warning() {
        val openDialog = remember { mutableStateOf(true) }
        if (openDialog.value) {
            AlertDialog(
                onDismissRequest = {},
                title = {
                    Text(text = "Note")
                },
                text = {
                    Text(
                        text = "The SERP providers here are meant for testing how Gugal interacts with SERP providers. " +
                               "To find out more about the selected SERP provider visit the \"Change credentials\" setting.\n\n" +
                               "The \"Change search engine\" setting will crash the app, and so will restarting setup using the " +
                               "developer options, so once you set a provider from here, you can only set a non-development one by clearing " +
                               "Gugal's data and going through setup again.")
                },
                confirmButton = {
                    TextButton(
                        onClick = {
                            openDialog.value = false
                        }
                    ) {
                        Text("Continue")
                    }
                },
                dismissButton = {
                    TextButton(
                        onClick = {
                            openDialog.value = false
                            finish()
                        }
                    ) {
                        Text("Exit")
                    }
                }
            )
        }
    }

    fun saveSerp(serpID: String, saveGugalVersion: Boolean = false) {
        // Although you can define your own key generation parameter specification, it's
        // recommended that you use the value specified here.
        val keyGenParameterSpec = MasterKeys.AES256_GCM_SPEC
        val mainKeyAlias = MasterKeys.getOrCreate(keyGenParameterSpec)

        val sharedPrefsFile = "gugalprefs"
        val sharedPreferences: SharedPreferences = EncryptedSharedPreferences.create(
            sharedPrefsFile,
            mainKeyAlias,
            applicationContext,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )

        with (sharedPreferences.edit()) {
            this.putString("serp", serpID)
            // Save the last Gugal version to not show the changelog again
            if (saveGugalVersion) this.putInt("lastGugalVersion", BuildConfig.VERSION_CODE)
            apply()
        }
    }
}