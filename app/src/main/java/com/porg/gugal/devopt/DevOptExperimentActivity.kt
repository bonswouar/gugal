/*
 *     DevOptExperimentActivity.kt
 *     Gugal
 *     Copyright (c) 2022 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.devopt

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.porg.gugal.Experiment
import com.porg.gugal.Global.Companion.custTabInResults
import com.porg.gugal.Global.Companion.gugalNews
import com.porg.gugal.Global.Companion.gugalNewsSetup
import com.porg.gugal.Global.Companion.resultGrid
import com.porg.gugal.Global.Companion.searchPages
import com.porg.gugal.MainActivity
import com.porg.gugal.ui.theme.GugalTheme
import com.porg.m3.settings.RegularSetting

class DevOptExperimentActivity : ComponentActivity() {

    val tick = "✔️"
    @OptIn(
        androidx.compose.material3.ExperimentalMaterial3Api::class
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            GugalTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colorScheme.background) {
                    Column(
                        modifier = Modifier.fillMaxSize().verticalScroll(rememberScrollState())
                    ) {
                        ExperimentSetting(
                            body = "Toggles the News tab, featuring a clean news feed powered by RSS, for this session.",
                            experiment = gugalNews
                        )
                        if (gugalNews.enabled) {
                            ExperimentSetting(
                                body = "Adds setup pages for news to the setup wizard.",
                                experiment = gugalNewsSetup
                            )
                        }
                        ExperimentSetting(
                            body = "If enabled, tapping on search results will open custom tabs instead of opening a tab in the browser.",
                            experiment = custTabInResults
                        )
                        ExperimentSetting(
                            body = "If enabled, the results are shown as a grid instead of a padded row. Only works on tablets/foldables and has no effect on phones.",
                            experiment = resultGrid
                        )
                        ExperimentSetting(
                            body = "Adds pages to the search result list.",
                            experiment = searchPages
                        )
                    }
                }
            }
        }
    }

    @OptIn(ExperimentalAnimationApi::class, ExperimentalMaterialApi::class)
    @Composable
    private fun ExperimentSetting(experiment: Experiment, body: String) {
        RegularSetting(
            title = createTitle(experiment),
            body = body,
            onClick = {
                experiment.enabled = !experiment.enabled
                var ed = "en"; if (!experiment.enabled) ed = "dis"
                doExtraActions(experiment)
                Toast.makeText(applicationContext, "${experiment.title} has been ${ed}abled for this session.",
                    Toast.LENGTH_SHORT).show()
                startActivity(Intent(applicationContext, MainActivity::class.java))
            },
        )
    }

    private fun doExtraActions(exp: Experiment) {
    }

    private fun createTitle(experiment: Experiment): String {
        return if (experiment.enabled) "${experiment.title} $tick"
        else experiment.title
    }
}