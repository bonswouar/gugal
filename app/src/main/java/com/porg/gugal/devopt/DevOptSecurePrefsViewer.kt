/*
 *     DevOptSecurePrefsViewer.kt
 *     Gugal
 *     Copyright (c) 2022 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.devopt

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import com.porg.gugal.Global.Companion.sharedPreferences
import com.porg.gugal.ui.theme.GugalTheme
import com.porg.m3.settings.RegularSetting

class DevOptSecurePrefsViewer  : ComponentActivity() {
    @OptIn(
        ExperimentalMaterialApi::class,
        androidx.compose.animation.ExperimentalAnimationApi::class,
        androidx.compose.material3.ExperimentalMaterial3Api::class
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            GugalTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colorScheme.background) {
                    Column(
                        modifier = Modifier.fillMaxSize()
                            .verticalScroll(rememberScrollState())
                    ) {
                        sharedPreferences.all.forEach { pref ->
                            RegularSetting(title = pref.key,
                                body = convert(pref),
                                onClick = { copyPref(pref) }
                            )
                        }
                    }
                }
            }
        }
    }

    private fun copyPref(pref: Map.Entry<String, Any?>) {
        // disallow copying of sensitive credentials
        if (pref.key.startsWith("serp_")) {
            Toast.makeText(this, "Sensitive credentials cannot be copied", Toast.LENGTH_SHORT)
                .show()
            return
        }
        val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("label","Preference ${pref.key}'s value is ${pref.value}")
        clipboard.setPrimaryClip(clip)
        Toast.makeText(this, "Copied to clipboard", Toast.LENGTH_SHORT)
            .show()
    }

    private fun convert(pref: Map.Entry<String, Any?>): String {
        if (pref.value == null) return "(null)"
        val prefString = pref.value.toString()
        // return sensitive credentials partially
        if (pref.key.startsWith("serp_")) {
            if (prefString.length < 4) return "(protected)"
            else if (prefString.length in 4..8) return prefString.substring(0, 4) + "..."
            return prefString.substring(0, 8) + "..."
        }
        return pref.value.toString()
    }
}