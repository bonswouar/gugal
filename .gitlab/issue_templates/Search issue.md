## What the issue is

(Summarize the problem encountered)

## Steps to reproduce

(How one can reproduce the issue - this is very important)

## What is the current behavior?

(What actually happens)

## What is the expected behavior?

(What you should see instead)

## Environment

Gugal version: (e.g. 0.4.1 - check the app's info)
Android version: (e.g. 12 - check your device's software information setting)
Device: (e.g. Google Pixel 5)
Query (if applicable): (e.g. cat photos - what you searched, if related to a specific query)

/label ~search-engine
