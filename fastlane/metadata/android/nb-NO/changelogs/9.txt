- Søk med searx eller SearXNG. Vær oppmerksom på at bare instanser med API-tilgang støttes. De fleste offentlige instanser har ikke API-tilgang aktivert, og det anbefales derfor å vertstjene searx eller SearXNG selv.
- Når Gugal startes for første gang etter en oppdatering vises en liste over endringene i oppdateringen.
- «Om»-side som erstatter flere innstillingselementer.
- Alternativ for å endre påloggingsinformasjonen.
- Russisk oversettelse.
- Bokmålsoversettelse, av Allan Nordhøy.
- Fikset rulling i innstillinger og resultater.
- Fikset at versjonsteksten på innstillingssiden var svart.
- Lyseblått fargevalg på Android 11 og tidligere.

For utviklere:
- Støtte for flere nettsøkemotorer og søk på enheten kan legges til med SERP-leverandører. Info om utvikling med SERP-leverandører finnes på gugal.gitlab.io/serp.
