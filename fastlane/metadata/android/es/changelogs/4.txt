- Se ha añadido un asistente de configuración al estilo Material 3, que sustituye a las tarjetas mostradas en el uso inicial.
- Más partes de la aplicación utilizan Material 3/You.
- Se ha añadido una página de configuración.
- Se ha añadido una opción para cambiar las credenciales del motor de búsqueda.
- Se ha corregido un fallo cuando un resultado no tiene descripción.
